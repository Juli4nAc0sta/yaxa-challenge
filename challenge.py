import argparse
from json import loads


def mas_cruciales(network, nodes):
    network_list = [x for i in network for x in i]
    network_count = {i: network_list.count(i) for i in network_list}
    network_sorted = sorted(network_count.items(), key=lambda kv: kv[1])
    node_points = sorted(list(set(nodes[i] for i in nodes)))[-1]
    [network_count.pop(i[0]) if network_sorted[-1][1] != i[1]
     else network_sorted for i in list(network_sorted)]
    [nodes.pop(i) if node_points != nodes[i] else nodes for i in dict(nodes)]
    result = (list(set(network_count) & set(nodes)))
    print(result if result else list(network_list[0]))

parser = argparse.ArgumentParser()
parser.add_argument(
    "--network", help="Connections between nodes, example: A,B B,C", nargs='+')
parser.add_argument(
    "--nodes",
    help="Users in the nodes, example: '{\"A\": 10, \"B\": 20}'", required=True)
args = parser.parse_args()
network, nodes = [i.split(',') for i in args.network], loads(args.nodes)
mas_cruciales(network, nodes)
