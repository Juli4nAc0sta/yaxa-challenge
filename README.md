# Yaxa Challenge

## Descripción

Los ciudadanos de GridLand se envían correos electrónicos entre
ellos todo el tiempo. Envían todo: lo que acaban de comer, una
imagen divertida, preguntas o pensamientos que los están molestando 
en este momento. Todos los ciudadanos están contentos porque tienen
una red tan maravillosa que los mantiene conectados.

El objetivo principal del alcalde es controlar la felicidad de
la ciudad. La felicidad de la ciudad es una suma de la felicidad 
de todos los ciudadanos. Y la felicidad de cada ciudadano es
igual a la cantidad de ciudadanos (siempre incluyéndose uno 
mismo) a los que se puede enviar correos electrónicos.

Debido a que la ciudad está creciendo, los ciudadanos han decidido 
que el Alcalde necesita un asistente para enfocarse en la protección del nodo.

Tu misión es descubrir cuáles serán los primeros nodos para 
investigar y proteger para el nuevo asistente. Recuerde, debe elegir el 
nodo más importante de la red. Si varios nodos tienen la máxima importancia, encuéntralos todos.

Entrada: 
    Dos argumentos: la estructura de la red (como una lista de conexiones 
    entre los nodos), los usuarios en cada nodo (según las claves donde
    las claves son los nombres de los nodos y los valores son las cantidades 
    de usuarios).

Salida: Lista de los nodos más cruciales.

Ejemplo:
```python
    mas_cruciales([['A', 'B'], ['B', 'C'] ],{'A': 10, 'B': 10, 'C': 10 }) == ['B']
    mas_cruciales([['A', 'B'] ],{'A': 20, 'B': 10 }) == ['A']
    mas_cruciales([['A', 'B'], ['A', 'C'], ['A', 'D'], ['A', 'E'] ],{'A': 0, 'B': 10, 'C': 10, 'D': 10, 'E': 10 }) == ['A']
    mas_cruciales([['A', 'B'], ['B', 'C'], ['C', 'D'] ],{'A': 10, 'B': 20, 'C': 10, 'D': 20 }) == ['B']
```

# Ejecución

```bash
    git clone https://gitlab.com/Juli4nAc0sta/yaxa-challenge
```

## Ejemplos
```bash
    $ python3 challenge.py --network A,B B,D C,A --node '{"A":20,"B":20,"C":2,"D":1}'
    $ ['B', 'A']
```

```bash
    $ python3 challenge.py --network A,B B,D C,A --node '{"A":10,"B":20,"C":2,"D":1}'
    $ ['B']
```

```bash
    $ python3 challenge.py --network A,B B,D C,A C,B --node '{"A":20,"B":20,"C":20,"D":1
    $ ['B']
```

```bash
    $ python3 challenge.py --network A,B B,D C,A C,D --node '{"A":20,"B":20,"C":20,"D":1}''
    $ ['C', 'A', 'B']
```

